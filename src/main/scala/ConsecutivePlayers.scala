import org.apache.commons.lang.time.DateUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}

import scala.util.{Failure, Success, Try}

/**
  * This can be run locally or in the terminal.
  * To run in terminal, go to root directory of the project and run command 'sbt run'. Then select [1] for ConsecutivePlayers when prompted.
  * Running in a Spark Cluster requires a few SparkConf setting changes.
  *
  * DataFrame states:
  *
  * Input DataFrame:
          root
           |-- activity_time: long (nullable = true)
           |-- user_id: string (nullable = true)

          +-------------+-----------+
          |activity_time|    user_id|
          +-------------+-----------+
          |   1451606400|11313534953|
          |   1451606400|12460277324|
          |   1451606400| 8174285381|
          +-------------+-----------+

  * Next DataFrame: Group all timestamps by a player's ID and collects all the timestamps into a List()

          root
           |-- user_id: string (nullable = true)
           |-- activity: array (nullable = true)
           |    |-- element: string (containsNull = true)

          +-----------+--------------------+
          |    user_id|            activity|
          +-----------+--------------------+
          |10740203242|[2016/01/01, 2016...|
          |10749656969|[2016/01/08, 2016...|
          |10766358962|[2016/01/01, 2016...|
          +-----------+--------------------+

  * Next DataFrame: For each player, map start dates to the number of consecutive days played afterwards based on the list of timestamps.
  * The consecutiveDays column contains a List(Map[Int,Int]). Each Map is a (startDate -> consecutiveDaysPlayed)

          root
           |-- user_id: string (nullable = true)
           |-- consecutiveDays: array (nullable = true)
           |    |-- element: struct (containsNull = true)
           |    |    |-- day: integer (nullable = false)
           |    |    |-- consecutiveDays: integer (nullable = false)
           |-- activity: array (nullable = true)
           |    |-- element: string (containsNull = true)

          +-----------+--------------------+--------------------+
          |    user_id|     consecutiveDays|            activity|
          +-----------+--------------------+--------------------+
          |10740203242|[[1,1], [3,2], [7...|[2016/01/01, 2016...|
          |10749656969|     [[1,8], [10,5]]|[2016/01/08, 2016...|
          |10766358962|             [[1,1]]|[2016/01/01, 2016...|
          +-----------+--------------------+--------------------+

  * Next DataFrame:
  *   Flatten each consecutiveDays List(Map[StartDate,ConsecutiveDaysPlayed])
  *   Group ConsecutiveDaysPlayed by StartDate and aggregate days played into a list, consecutiveDaysFromStartDate.
  *   This list represents records of consecutive days played after starting on a StartDate.
  *   For every record in consecutiveDaysFromStartDate, aggregate the frequency of each consecutive day into a Map.
  *     Now for each startDate, we have a Map[consecutiveDaysPlayed, numberOfPlayers]

          root
           |-- startDate: integer (nullable = true)
           |-- consecutiveDaysFromStartDate: array (nullable = true)
           |    |-- element: integer (containsNull = true)
           |-- playerCounts: map (nullable = true)
           |    |-- key: integer
           |    |-- value: integer (valueContainsNull = false)

          +---------+----------------------------+--------------------+
          |startDate|consecutiveDaysFromStartDate|        playerCounts|
          +---------+----------------------------+--------------------+
          |        1|        [1, 8, 1, 1, 1, 4...|Map(5 -> 324, 10 ...|
          |        2|        [1, 2, 1, 1, 2, 1...|Map(5 -> 60, 10 -...|
          |        3|        [2, 1, 1, 1, 1, 1...|Map(5 -> 65, 10 -...|
          +---------+----------------------------+--------------------+

  * Final DataFrame:
  *   For each startDate, create a column for USERS_PLAYING_N_DAYs where N is a range of consecutive days from 1 to 14.
  *   Uses the Map[consecutiveDaysPlayed, numberOfPlayers] to populate the columns.
  *     In this Map, the Key is N (1-14) - consecutive days played - and the Value is the number of players found playing consecutively for N Days

          root
           |-- startDate: integer (nullable = true)
           |-- USERS_PLAYING_1_DAY: integer (nullable = true)
           |-- USERS_PLAYING_2_DAY: integer (nullable = true)
           |-- USERS_PLAYING_3_DAY: integer (nullable = true)
           |-- USERS_PLAYING_4_DAY: integer (nullable = true)
           |-- USERS_PLAYING_5_DAY: integer (nullable = true)
           |-- USERS_PLAYING_6_DAY: integer (nullable = true)
           |-- USERS_PLAYING_7_DAY: integer (nullable = true)
           |-- USERS_PLAYING_8_DAY: integer (nullable = true)
           |-- USERS_PLAYING_9_DAY: integer (nullable = true)
           |-- USERS_PLAYING_10_DAY: integer (nullable = true)
           |-- USERS_PLAYING_11_DAY: integer (nullable = true)
           |-- USERS_PLAYING_12_DAY: integer (nullable = true)
           |-- USERS_PLAYING_13_DAY: integer (nullable = true)
           |-- USERS_PLAYING_14_DAY: integer (nullable = true)

          +---------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
          |startDate|USERS_PLAYING_1_DAY|USERS_PLAYING_2_DAY|USERS_PLAYING_3_DAY|USERS_PLAYING_4_DAY|USERS_PLAYING_5_DAY|USERS_PLAYING_6_DAY|USERS_PLAYING_7_DAY|USERS_PLAYING_8_DAY|USERS_PLAYING_9_DAY|USERS_PLAYING_10_DAY|USERS_PLAYING_11_DAY|USERS_PLAYING_12_DAY|USERS_PLAYING_13_DAY|USERS_PLAYING_14_DAY|
          +---------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
          |        1|               4283|               1533|                781|                401|                324|                226|                162|                204|                205|                 154|                 101|                  93|                  74|                1290|
          |        2|               2830|                503|                219|                 89|                 60|                 37|                 26|                 23|                 30|                  13|                  15|                  11|                  72|                   0|
          |        3|               2971|                554|                206|                101|                 65|                 55|                 36|                 32|                 20|                  14|                  10|                  94|                   0|                   0|
          +---------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+--------------------+--------------------+--------------------+--------------------+--------------------+

  * Created by timothyshen on 8/27/17.
  */
object ConsecutivePlayers {

  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  val SCHEMA: StructType = StructType(
    StructField("activity_time", LongType) ::
      StructField("user_id", StringType) ::
      Nil
  )
  val SCHEMA2: StructType = StructType(
    StructField("day", IntegerType) ::
      StructField("count", IntegerType) ::
      Nil
  )
  val DAY_FORMAT = "yyyy/MM/dd"
  val (lowerEpoch: Long, upperEpoch: Long) = ( // Had this here to filter out
    new DateTime().withYear(2016).withMonthOfYear(1).withDayOfMonth(1).withZone(DateTimeZone.UTC).getMillis,
    new DateTime().withYear(2016).withMonthOfYear(1).withDayOfMonth(14).withZone(DateTimeZone.UTC).getMillis
  )


  private def getInfo(description: String, activityDf: DataFrame) = {
    println("\n\n=============================================================================================================================================================================================================================")
    println(s"Showing info for\t $description")
    println("=============================================================================================================================================================================================================================")
    activityDf.printSchema()
    activityDf.show()
  }

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder
      .appName("ConsecutivePlayers")
      .master("local[*]")
      .getOrCreate()

    import spark.sqlContext.implicits._
    spark.sparkContext.setLogLevel("OFF")

    val inputDataPath = Try(args(0)) match {
      case Success(inputPath) => inputPath
      case Failure(e) => "activity_data_2016_01_01.txt.gz"
    }

    val activityDf: DataFrame = spark.read.schema(SCHEMA).csv(inputDataPath)
//    getInfo("Initial Dataframe", activityDf)

    val playerActivityDf: DataFrame = activityDf
      .withColumn("activity_time", convertEpochSecondsToMilliseconds('activity_time))
      .withColumn("day", convertEpochToDateFormat('activity_time))
      .select('user_id, 'day)
      .groupBy('user_id).agg(collect_list('day).as('activity))
//    getInfo("DataFrame with activity timestamps grouped by user ids", playerActivityDf)

    val playerConsecutiveDaysDf = playerActivityDf
      .withColumn("consecutiveDays", calculateStartDateAndConsecutiveDaysPlayed('activity))
      .select('user_id, 'consecutiveDays, 'activity)
//    getInfo("DataFrame with player and their start dates for consecutive days of playing in a Map", playerConsecutiveDaysDf)

    val dateAndConsecutivePlays = playerConsecutiveDaysDf
      .withColumn("dayToConsecutiveCount", explode('consecutiveDays))
      .withColumn("startDate", extractDay('dayToConsecutiveCount))
      .withColumn("consecutiveDays", extractConsecutiveDays('dayToConsecutiveCount))
      .select('startDate, 'consecutiveDays)
      .groupBy('startDate).agg(collect_list('consecutiveDays).as("consecutiveDaysFromStartDate"))
      .withColumn("playerCounts", aggregatePlayerCountsPerConsecutiveDays('consecutiveDaysFromStartDate))
      .sort('startDate)
//    getInfo("DataFrame with a start date and all #s of consecutive days found for the start date", dateAndConsecutivePlays)

    val finalDf = getNumberOfConsecutivePlayersPerDay(dateAndConsecutivePlays)
    getInfo("Resulting dataframe: Date and number of users playing for consecutive dates from 1,14", finalDf)

    finalDf.coalesce(1).write.mode("OVERWRITE").csv("./result")

  }

  def getNumberOfConsecutivePlayersPerDay(df: DataFrame): DataFrame = {
    import df.sqlContext.implicits._
    df.withColumn("USERS_PLAYING_1_DAY", lookupPlayerCountByConsecutiveDays(1)('playerCounts))
      .withColumn("USERS_PLAYING_2_DAY", lookupPlayerCountByConsecutiveDays(2)('playerCounts))
      .withColumn("USERS_PLAYING_3_DAY", lookupPlayerCountByConsecutiveDays(3)('playerCounts))
      .withColumn("USERS_PLAYING_4_DAY", lookupPlayerCountByConsecutiveDays(4)('playerCounts))
      .withColumn("USERS_PLAYING_5_DAY", lookupPlayerCountByConsecutiveDays(5)('playerCounts))
      .withColumn("USERS_PLAYING_6_DAY", lookupPlayerCountByConsecutiveDays(6)('playerCounts))
      .withColumn("USERS_PLAYING_7_DAY", lookupPlayerCountByConsecutiveDays(7)('playerCounts))
      .withColumn("USERS_PLAYING_8_DAY", lookupPlayerCountByConsecutiveDays(8)('playerCounts))
      .withColumn("USERS_PLAYING_9_DAY", lookupPlayerCountByConsecutiveDays(9)('playerCounts))
      .withColumn("USERS_PLAYING_10_DAY", lookupPlayerCountByConsecutiveDays(10)('playerCounts))
      .withColumn("USERS_PLAYING_11_DAY", lookupPlayerCountByConsecutiveDays(11)('playerCounts))
      .withColumn("USERS_PLAYING_12_DAY", lookupPlayerCountByConsecutiveDays(12)('playerCounts))
      .withColumn("USERS_PLAYING_13_DAY", lookupPlayerCountByConsecutiveDays(13)('playerCounts))
      .withColumn("USERS_PLAYING_14_DAY", lookupPlayerCountByConsecutiveDays(14)('playerCounts))
      .select('startDate,
        'USERS_PLAYING_1_DAY,
        'USERS_PLAYING_2_DAY,
        'USERS_PLAYING_3_DAY,
        'USERS_PLAYING_4_DAY,
        'USERS_PLAYING_5_DAY,
        'USERS_PLAYING_6_DAY,
        'USERS_PLAYING_7_DAY,
        'USERS_PLAYING_8_DAY,
        'USERS_PLAYING_9_DAY,
        'USERS_PLAYING_10_DAY,
        'USERS_PLAYING_11_DAY,
        'USERS_PLAYING_12_DAY,
        'USERS_PLAYING_13_DAY,
        'USERS_PLAYING_14_DAY)
      .sort('startDate)
  }

  def lookupPlayerCountByConsecutiveDays(numberOfConsecutiveDays: Int) = udf((input: Map[Int, Int]) => input.getOrElse(numberOfConsecutiveDays, default = 0))

  case class DaysAndNumberOfUsers(consecutiveDays: Int, numUsers: Int)

  def aggregatePlayerCountsPerConsecutiveDays = udf((counts: Seq[Int]) => {
    counts.groupBy(identity)
      .mapValues(_.length)
      .map(result => result._1 -> result._2)
  })

  case class DayAndCount(day: Int, consecutiveDays: Int)

  def extractDay = udf((dayToCount: Row) => dayToCount.getAs[Int]("day"))

  def extractConsecutiveDays = udf((dayToCount: Row) => dayToCount.getAs[Int]("consecutiveDays"))

  def calculateStartDateAndConsecutiveDaysPlayed: UserDefinedFunction = udf((timestamps: Seq[String]) => {
    var userConsecutiveResults: Map[Int, Int] = Map()

    val activeDays: Seq[DateTime] = convertStringsToDateTimes(timestamps)
    var targetDate = activeDays.head.plusDays(1)
    var currentConsecutiveCount = 1
    var consecutiveStartDate = activeDays.head

    def updateResultMap() = {
      val startDateString = consecutiveStartDate.dayOfMonth().get()
      userConsecutiveResults += (startDateString -> currentConsecutiveCount)
    }

    activeDays.drop(1).foreach {
      day =>
        if (DateUtils.isSameDay(day.toDate, targetDate.toDate)) {
          currentConsecutiveCount += 1
          targetDate = day.plusDays(1)
        }
        else {
          updateResultMap()
          consecutiveStartDate = day
          currentConsecutiveCount = 1
          targetDate = day.plusDays(1)
        }
    }
    updateResultMap()

    userConsecutiveResults
      .map(result => DayAndCount(result._1, result._2))
      .toList
  })

  def convertStringsToDateTimes(timestamps: Seq[String]) = {
    val activeDays: Seq[DateTime] = timestamps
      .map(DateTimeFormat.forPattern(DAY_FORMAT).parseDateTime(_))
      .distinct
      .sortBy(_.getMillis)
    activeDays
  }

  def convertEpochSecondsToMilliseconds: UserDefinedFunction = udf((epochSeconds: Long) => epochSeconds * 1000)

  def convertEpochToDateFormat: UserDefinedFunction = udf((epoch: Long) => new DateTime(epoch).withZone(DateTimeZone.UTC).toString(DAY_FORMAT)) // Converted to Strings for DataFrame Compatibility
}
