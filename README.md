# README #

Exercise to determine, per date, how many players played for N number of consecutive days for the dates 2016/01/01 to 2016/01/14.

Main Class: ConsecutivePlayers.scala

* This class takes a parameter for the path to the data. If not provided it will default to the .csv saved in the project.
	* Default .csv is the same file provided with the challenge.
* JavaDocs contains description of the logic flow for this class.
* In ConsecutivePlayers.scala there are commented out lines for '// getInfo()' method calls.
	* Uncomment these lines to have information about each DataFrame printed along the stages of execution.
	* Uncommenting getInfo() method calls will double the run time.
	
* Resulting file is written into the root directory of the project, under a folder 'result/'

### Dependencies ###

SBT v0.13.16 for Dependency management.

Project uses:

* Scala 2.11.11
* Spark 2.2.0


### Setup ###

* 'git clone https://tshezzy@bitbucket.org/tshezzy/consecutiveplayers.git'
* 'cd ~/path/to/repo'
* 'sbt run'
* select [1] to run ConsecutivePlayers Object.

OR

* 'git clone https://tshezzy@bitbucket.org/tshezzy/consecutiveplayers.git'
* Open in IDE
* Import project as SBT project
* Run ConsecutivePlayers Object

### Who do I talk to? ###
* Tim Shen
* timshen1116@gmail.com or 858 703 7000