import ConsecutivePlayers.DaysAndNumberOfUsers
import org.apache.commons.lang.time.DateUtils
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.joda.time.{DateTime, DateTimeZone, Days}
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable.ArrayBuffer

/**
  * Created by timothyshen on 8/27/17.
  */
object Testbench extends App {
  val it = Array(1,2,1,2,3,4,5,5,5,10,9)
  def doStuff(counts: Array[Int]) = {
    counts.groupBy(identity)
      .mapValues(_.length)
      .map(result => DaysAndNumberOfUsers(result._1, result._2))
      .toList
  }

  doStuff(it).foreach(println)
}
